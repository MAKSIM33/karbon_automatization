import vk_api
from vk_api.keyboard import VkKeyboard
import random
import os
import dotenv

token = '53590ee6949c91087ad6824f0f9464dab55fbe5e23a751f8d89c9cc5b3613bb66205b4ff66936fd56e7db'


env_path = dotenv.find_dotenv()
dotenv.load_dotenv(dotenv_path=env_path)
bot_token = os.getenv('BOT_TOKEN')

token = bot_token


SCOROHODOV = os.getenv('SCOROHODOV')
CHETVERIKOV = os.getenv('CHETVERIKOV')
USTUZHANIN = os.getenv('USTUZHANIN')
PUGACHEV = os.getenv('PUGACHEV')
DOLGOPOLOVA = os.getenv('DOLGOPOLOVA')
SHARGORODSKIY = os.getenv('SHARGORODSKIY')
RUDIH = os.getenv('RUDIH')
SVINKIN = os.getenv('SVINKIN')
PYATKOVA = os.getenv('PYATKOVA')
KARPOV = os.getenv('KARPOV')
AUSHEEVA = os.getenv('AUSHEEVA')
MOSKALUK = os.getenv('MOSKALUK')
SAVATEEVA = os.getenv('SAVATEEVA')
VOLOSATOVA = os.getenv('VOLOSATOVA')
HABIBULIN = os.getenv('HABIBULIN')
AUSHEEV = os.getenv('AUSHEEV')


vk = vk_api.VkApi(token=token)
vk._auth_token()


def clear_text(text):
    """Функция которая приводит все буквы в нижний регистр и убирает лишнее"""
    text = text.lower()
    text = ''.join(
        [char for char in text if char in 'абвгдеёжзийклмнопрстуфхцчшщъюьэюя qwertyuiopasdfghjklzxcvbnm-0123456789'])
    return text


def send_question(user_id, message, keyboard=None):
    """Функция которая отправляет вопрос"""
    if keyboard:
        vk.method('messages.send', {'user_id': user_id,
                                    'message': message,
                                    'random_id': random.randint(1, 1000),
                                    'keyboard': keyboard.get_keyboard()
                                    })
    else:
        vk.method('messages.send', {'user_id': user_id,
                                    'message': message,
                                    'random_id': random.randint(1, 1000),
                                    })
        

def generate_keyboard(list_buttons, number_of_question):
    '''Функция которая создает кнопки для вопроса'''
    buttons = list_buttons[number_of_question]
    list_buttons = []
    dict_keys = buttons.keys()
    for key in dict_keys:
        list_buttons.append(key)
    keyboard = VkKeyboard(one_time=True)
    flag = False
    for button in list_buttons:
        if button:
            keyboard.add_button(button)
            flag = True
    if flag:    
        return keyboard
    else:
        return None

def send_invitation(id_users):
    for id_man in list_ids:
        keyboard_for_first_message = VkKeyboard(one_time=True)
        keyboard_for_first_message.add_button('Начать')

        send_question(id_man, 
                      'Доброго времени суток! Для вас появился новый опрос! Нажмите "Начать" чтобы запустить опрос', 
                      keyboard=keyboard_for_first_message)
        
        
def generate_information_about_staff(list_ids):
    
    info_about_users_and_number_of_question = {}
    
    for id_man in list_ids:
        info_about_users_and_number_of_question[id_man] = 0 
        
    return info_about_users_and_number_of_question

class Poll():
    """Этот парень, создает необходимую информацию об участниках опроса"""
    def __init__(self, list_questions, list_anwers, list_id_users):
        
        # Инициализируем входные данные
        
        self.list_questions = list_questions
        self.list_anwers = list_anwers
        self.list_id_users = list_id_users
        
        # Создаем персоналную информацию о пользвателеях в списке словарей
        # Ключ - id пользователя, значение - номер предыдущего заданного вопроса
        
        self.user_id_and_number_of_question = generate_information_about_staff(self.list_id_users)
        
        # Отправляем приглашение пройти опрос
        
        send_invitation(self.list_id_users)
        
        # создаем словарь для хранения предыдущего вопроса конкретному пользователю
        
        self.history_of_question = {}
        
    def acceptable_answer(self, answer, user_id):
        """Главный чел, который возвращает нам, на какой вопрос ответил участник опроса
            Так же он отравляет следующие вопросы пользователю 
            и ведет ту информацию, которую мы задали на стадии инициализирования"""
            
        # Если текст пользователя не равен начать и этот пользователь есть в списке опрашиваемых
        
        if answer != 'начать' and user_id in self.user_id_and_number_of_question and self.user_id_and_number_of_question[user_id] != 0:
            
            # Сразу возьмем предыдущий вопрос этому пользователю
            
            question = self.history_of_question[user_id]
            
            # Проверяем, весь ли опрос прошел пользователь
            # Если да, то отправляем ему следующее сообщение
            # Если нет, переходим дальше
            
            if self.user_id_and_number_of_question[user_id] > len(self.list_questions):
                
                message = 'Вы уже прошли данный опрос! Возвращайтесь позднее'
                
                send_question(user_id, message)
                
            else:
                
                # Если ответ содержится в списке заранее подготовленных
                
                if answer in self.list_anwers[self.user_id_and_number_of_question[user_id] - 1]:
                    
                    # Берем значение ДОПОЛНИТЕЛЬНОГО ВОПРОСА
                    
                    additional_message = self.list_anwers[self.user_id_and_number_of_question[user_id] - 1][answer]

                    # Если ДОПОЛНИТЕЛЬНЫЙ ВОПРОС есть, тогда задаем его
                    
                    if additional_message:
                            
                        message = additional_message
                        send_question(user_id, message)
                            
                        # Если дополнительный вопрос НЕ Введите ваш ответ
                        # Сохраняем этот вопрос как предыдущий
                        if additional_message != 'Введите ваш ответ':

                            self.history_of_question[user_id] = additional_message
                        
                        # Если ответ не Другой ответ, возвращаем предыдущий вопрос
                        # Если ответ Другой ответ, тогда возвращаем None
                        # Что говорит нам о необходимости уточнить следующий ответ
                        
                        if answer != 'Другой ответ':
                            return question
                        else:
                            return None
                        
                    else:
                        # Для заранее НЕ заданных ответов
                        #  Проверяем, не последний ли это вопрос
                        # Если да, то отправляем Спасибо за прохождение опроса
                        # И возвращаем вопрос, на который ответил пользователь
                       
                        if self.user_id_and_number_of_question[user_id] == len(self.list_questions):

                            message = 'Спасибо за прохождение опроса'
                                
                            send_question(user_id, message)
                                
                            self.user_id_and_number_of_question[user_id] += 1   
                            
                            return question
                            
                        else:
                            #  Если это вопрос не последний, задаем ему следующий
                            # И возвращаем вопрос, на который ответил пользователь 
                            
                            keyboard_for_this_question = generate_keyboard(self.list_anwers,
                                                                                       self.user_id_and_number_of_question[user_id])

                            message = self.list_questions[self.user_id_and_number_of_question[user_id]]
                                
                            send_question(user_id, message, keyboard_for_this_question)
                                
                            self.history_of_question[user_id] = message
    
                            self.user_id_and_number_of_question[user_id] += 1
                                
                            return question
                    
                else:
                    # Для заранее заданных ответов
                    # Проверяем, не последний ли это вопрос
                    # Если да, то отправляем Спасибо за прохождение опроса
                    # И возвращаем вопрос, на который ответил пользователь
                    
                    if self.user_id_and_number_of_question[user_id] == len(self.list_questions):
                    
                        message = 'Спасибо за прохождение опроса' 
                        
                        send_question(user_id, message)
                            
                        self.user_id_and_number_of_question[user_id] += 1
                        
                        return question
                        
                    else:
                        
                        #  Если это вопрос не последний, задаем ему следующий
                        # И возвращаем вопрос, на который ответил пользователь 
                        
                        keyboard_for_this_question = generate_keyboard(self.list_anwers,
                                                                       self.user_id_and_number_of_question[user_id])
                            
                        message = self.list_questions[self.user_id_and_number_of_question[user_id]] 
                        
                        send_question(user_id, message, keyboard_for_this_question)     
                        
                        self.history_of_question[user_id] = message
    
                        self.user_id_and_number_of_question[user_id] += 1
                        
                        return question
        # Если текст равнен начать и этот пользователь есть в списках
        # Отправляем ему первый вопрос и запоминаем необходимую информацию
        # return None для того, чтобы выйти из метода, т.к. это был первый вопрос
        
        elif answer == 'начать' and user_id in self.user_id_and_number_of_question:
            if self.user_id_and_number_of_question[user_id] < len(self.list_questions):
                message = self.list_questions[self.user_id_and_number_of_question[user_id]]
    
                keyboard_for_this_question = generate_keyboard(self.list_anwers, 0)
                
                send_question(user_id, message, keyboard_for_this_question)
                
                self.history_of_question[user_id] = message
                    
                self.user_id_and_number_of_question[user_id] += 1
                
                return None



def start_pool(list_questions, list_anwers, list_id_users, dict_for_tabel):
    """Начинает опрос"""
    
    # Создаем экземпляр выше написанного класса
    
    poll = Poll(list_questions, list_anwers, list_id_users)
    
    # ждем сообщений в цикле
    while True:
        messages = vk.method('messages.getConversations',
                            {'offset': 0, 'count': 20, 'filter': 'unanswered'})

        if messages['count'] > 0:
            
            user_id = messages['items'][0]['last_message']['from_id']
            
            text = messages['items'][0]['last_message']['text']
            # Если сообщение пришло, прогоняем его через метод класса Poll
            # Который возвращает вопрос, на который ответил пользователь
            
            question = poll.acceptable_answer(text, user_id)
            
            if question in dict_for_tabel:
                # Для добавления в базу данных
                name_for_tabel = dict_for_tabel[question]
                print(user_id, name_for_tabel, text)
            
# Список вопросов для опроса

list_questions = ['Евгений Рудых отправлял в беседу НО Карбон список ближайших мероприятий по науке.\nНеобходимо в ОДНОМ сообщении написать, где будете участвовать.',
                  ]
dict_for_tabel = {'Евгений Рудых отправлял в беседу НО Карбон список ближайших мероприятий по науке.\nНеобходимо в ОДНОМ сообщении написать, где будете участвовать.':'conf',
                  }
# Список списков ответов для кнопок
# должен строго соответствовать каждому вопросу в предидушем списке

answers = [{None:None}]

# Список id участников опроса
list_ids = [int(CHETVERIKOV), int(PUGACHEV)]

start_pool(list_questions, answers, list_ids, dict_for_tabel)
