import mysql.connector
import dotenv
import os


env_path = dotenv.find_dotenv()
dotenv.load_dotenv(dotenv_path=env_path)
user = os.getenv('DB_USER')
passwd = os.getenv('DB_PASS')


mydb = mysql.connector.connect(
    host="localhost",
    user=user,
    password=passwd,
    database='bot_karbon',
)

mycursor = mydb.cursor()


def insert_sql(table_name, value, field):
    sql = f"INSERT INTO %s (%s) VALUES (%s)"
    val = (table_name, field, value)
    print(table_name, field, value)
    print(type(table_name), type(field), type(value))
    mycursor.execute(sql, val)
    mydb.commit()


def update_sql(table_name, field_name, value, user_id):
    s = f"UPDATE {table_name} SET {field_name}='{value}' WHERE user_id={user_id}"
    print(s)
    if s=="UPDATE answers SET meeting='да' WHERE user_id=178717155":
        print('true')
    mycursor.execute(s)
    mydb.commit()
